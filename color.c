#include "grep_options.h"
void color(char *str,char *search){
	char *c_start, *c_end, h, *print_head;
	c_start = strstr(str,search);
	print_head = str;
	c_end = c_start + strlen(search);
	while(print_head < c_start){
		h = *print_head++;
		printf(RESET_COLOR"%c",h);
	}
	while(print_head < c_end){
		h = *print_head++;
		printf(MAKE_RED"%c",h);
	}
	while(*print_head){
		h = *print_head++;
		printf(RESET_COLOR"%c",h);
	}
	printf("\n");
}
