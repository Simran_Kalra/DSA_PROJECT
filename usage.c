#include "grep_options.h"
void usage(int argc,char *argv[]){
	if(argc >= 2 && (strcmp(argv[1],"--help") == 0 || strcmp(argv[1],"--h") == 0)){
		printf("GREP Written By: SIMRAN KALRA\n");
		printf("Usage: grep [OPTION]... PATTERN [FILE]...\n");
		printf("Search for PATTERN in each FILE or standard input.\n");
		printf("PATTERN is, by default, a basic regular expression (BRE)\n");
		printf("Example: grep -i 'hello world' menu.h main.c\n");
		printf("OPTION list:\n");
		printf("  -r  : Read all files under each directory, recursively.\n");
		printf("  -i  : Ignore case distinctions in both the pattern and the input files\n");
		printf("  -v  : Invert the sense of matching, to select nonmatching lines\n");
		printf("  -f  : Obtain the pattern from file.\n");
		printf("  -w  : Select only those lines containing matches that form whole words.\n");
		printf("  -c  : Suppress normal output; instead print a count of matching lines for each input file\n");
		printf("  -m  : Stop reading a file after NUM matching lines.\n");
		printf("  -b  : Print the byte offset within the input file before each line of output.\n");
		printf("  -q  : Quiet; suppress normal output.\n");
		printf("  -n  : Prefix each line of output with the line number within its input file.\n");
		printf("  -H  : Print the file name for each match.\n");
		printf("  -h  : Suppress the prefixing of filenames on output when multiple files are searched.\n");
		printf("  -e  : Use pattern as the pattern; useful to protect patterns beginning with –\n");
		printf("  -iv : Insensitive & Invert combined together.\n");
		printf("  -in : Insensitive & prefix output with line number.\n");
		
	}
	else{
		printf("Usage: grep [OPTION]... PATTERN [FILE]...\n");
		printf("Try 'grep --help' for more information.");
	
	}
}

