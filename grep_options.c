#include "grep_options.h"
//-b Print the byte offset within the input file before each line of output.
void find_offset(int argc,char *argv[]){
	int j = 0, size = 100, c = 0, line_no = 0, m = 0, a[1000];
	char ch, *str, *search;
	int fd = open(argv[3],O_RDONLY);
	if(fd == -1) {
		perror("cp: can't open file");
		exit(errno);
	}
	str = (char *)malloc(sizeof(char)*size);
	search = (char *)malloc(sizeof(char)*size);
	strcpy(search,argv[2]);
	while(read(fd,&ch,sizeof(ch))){
		if(ch != '\n'){
			str[j] = ch;
			j++;
			if(j>=size-1){
				size=size*2;
				str=(char *)realloc(str,size);
			}
			c++;
		}
		else{
			line_no = line_no + 1;
			str[j] = '\0';
			j = 0;
			a[m] = 0;
			c++;
			a[m++] = c;
			if(line_no == 1){
				if(strstr(str,search) != NULL){
					printf(MAKE_GREEN "%d" MAKE_BLUE":" RESET_COLOR"%s\n",a[m-2],str);
				}	
				
			}
			else{
				if(strstr(str,search) != NULL){
					printf(MAKE_GREEN "%d" MAKE_BLUE":"RESET_COLOR,a[m-2]);
					color(str,search);
				}
			}
		}
	}
}

//-e Search multiple pattern in File  
void find_mpattern(int argc,char *argv[],int eflag,int f){
	char *line, *pat2, *pattern, ch;
	int fd;
	int size = 100, j = 0;
	pattern = (char *)malloc(sizeof(char)*size);
	pat2 = (char *)malloc(sizeof(char)*size);
	line = (char *)malloc(sizeof(char)*size);
	if(f == 1){
		fd = open(argv[5],O_RDONLY);
		strcpy(pattern,argv[3]);
		strcpy(pat2,argv[4]);
	}
	else{
		fd = open(argv[3],O_RDONLY);
		strcpy(pattern,argv[2]);
	}
	if(fd == -1) {
		perror("cp: can't open file");
		exit(errno);
	}
	
	while(read(fd,&ch,sizeof(char))){
		if(ch != '\n'){
			line[j] = ch;
			j++;
			if(j >= size-1){
				size = size * 2;
				line = (char *)realloc(line,size);
			}
		}
		else{
			line[j] = '\0';
			j = 0;
			if(f == 1){
				if(strstr(line,pattern)!=NULL || strstr(line,pat2)!=NULL){
					printf("%s\n",line);
				}
			}
			else{
				if(strstr(line,pattern)){
					color(line,pattern);
				}
				
			}
		}
	}
	free(line);
	close(fd);
}
 
//-H  Print the file name for each match
void find_match(int argc,char *argv[],int Hflag){
	char *line, *pattern, ch, *ret;
	int fd;
	int size = 100, j = 0;
	pattern = (char *)malloc(sizeof(char)*size);
	line = (char *)malloc(sizeof(char)*size);
	if(Hflag == 1){
		fd = open(argv[3],O_RDONLY);
		strcpy(pattern,argv[2]);
	}
	else{
		fd = open(argv[2],O_RDONLY);
		strcpy(pattern,argv[1]);
	}
	if(fd == -1) {
		perror("cp: can't open file");
		exit(errno);
	}
	while(read(fd,&ch,sizeof(char))){
		if(ch != '\n'){
			line[j] = ch;
			j++;
			if(j >= size-1){
				size = size * 2;
				line = (char *)realloc(line,size);
			}
		}
		else{
			line[j] = '\0';
			j = 0;
			if(Hflag == 1){
				if((ret = strstr(line,pattern))){
					printf(MAKE_MAGENTA"%s"MAKE_BLUE":",argv[3]);
					color(line,pattern);
				}
			}
			else{
				if(strstr(line,pattern)){
					color(line,pattern);	
				}	
					
			}
		}
	}
	free(line);
	close(fd);
}

//-i -in -iv Ignore case distinctions in both the pattern and the input files
void find_insensitive(int argc,char *argv[],char flag){
	char *line, *pattern, ch, *lline, num = 0;
	int fd;
	int size = 100, j = 0, l, i, ll;
	fd = open(argv[3],O_RDONLY);
	if(fd == -1) {
		perror("cp: can't open file");
		exit(errno);
	}
	pattern = (char *)malloc(sizeof(char)*size);
	strcpy(pattern,argv[2]);
	l = strlen(pattern);
	line = (char *)malloc(sizeof(char)*size);
	for(i = 0; i <= l; i++){
		if(pattern[i] >= 'a' && pattern[i] <= 'z'){
			pattern[i] = pattern[i] - 32;
	    	}
	}
	while(read(fd,&ch,sizeof(char))){
		if(ch != '\n'){
			line[j] = ch;
			j++;
			if(j >= size - 1){
				size = size * 2;
				line = (char *)realloc(line,size);
			}
		}
		else{
			line[j] = '\0';
			num++;
			ll = strlen(line);
			lline = (char *)malloc(sizeof(char) * ll);
			strcpy(lline,line);
			for(i = 0; i < ll; i++){
				if(line[i] >= 'a' && line[i] <= 'z'){
					line[i] = line[i] - 32;
				}
			}
			if(flag != 'v'){
				if(strstr(line,pattern)){
					if(flag == 'n'){
						printf(MAKE_GREEN"%d"MAKE_BLUE":"RESET_COLOR"%s\n",num,lline);
					}
					else{
						printf("%s\n",lline);
					}
				}
			}
			else{
				if(!strstr(line,pattern)){
					printf("%s\n",lline);
				}
			}
			j = 0;
		} 
	}
	free(line);
	close(fd);
}



//-v Invert the sense of matching, to select nonmatching lines
void find_invert(int argc,char *argv[],char flag){
	char *line, *pattern, ch;
	int fd;
	int size = 100, j = 0, ln = 0, c = 0, cflag=0;
	pattern = (char *)malloc(sizeof(char)*size);
	strcpy(pattern,argv[2]);
	fd = open(argv[3],O_RDONLY);
	if(fd == -1) {
		perror("cp: can't open file");
		exit(errno);
	}
	line = (char *)malloc(sizeof(char)*size);
	while(read(fd,&ch,sizeof(char))){
		if(ch != '\n'){
			line[j] = ch;
			j++;
			if(j >= size - 1){
				size = size * 2;
				line = (char *)realloc(line,size);
			}
		}
		else{
			line[j] = '\0';
			j = 0;
			ln++;
			if(flag=='c'){
				if(!strstr(line,pattern)){
					c=c+1;
					cflag=1;
				}
			}
			else if(flag=='n'){
				if(!strstr(line,pattern)){
					printf(MAKE_GREEN"%d"MAKE_BLUE":"RESET_COLOR"%s\n",ln,line);
				}	
			}
			else{
				if(!strstr(line,pattern)){
					printf("%s\n",line);
				}
			}		
		}
	}
		if(cflag==1)
			printf("%d\n",c);
	free(line);
	close(fd);
}

//-v -n -c Multiple Files
void find_mf(int argc,char *argv[],char opt){
	char *line, *pattern, ch;
	int fd, ln = 0;
	int size = 100, j = 0, i, cnt;
	pattern = (char *)malloc(sizeof(char)*size);
	strcpy(pattern,argv[2]);
	i = 3;
	while(i != argc){
		cnt = 0;
		ln = 0;
		fd = open(argv[i],O_RDONLY);
		if(fd == -1) {
		    perror("cp: can't open file");
		    exit(errno);
	    }
		line = (char *)malloc(sizeof(char)*size);
		while(read(fd,&ch,sizeof(char))){
			if(ch != '\n'){
				line[j] = ch;
				j++;
				if(j >= size - 1){
					size = size * 2;
					line = (char *)realloc(line,size);
				}
			}
			else{
				line[j] = '\0';
				j = 0;
				ln++;
				if(opt == 'v'){
					if(!strstr(line,pattern))
						printf(MAKE_MAGENTA"%s"MAKE_BLUE":"RESET_COLOR"%s\n",argv[i],line);
				}
				else if(opt == 'n'){
					if(strstr(line,pattern)){
						printf(MAKE_MAGENTA"%s" MAKE_BLUE ":" MAKE_GREEN "%d" MAKE_BLUE":",argv[i],ln);
						color(line,pattern);
					}		
				}
				else if(opt == 'c'){
					if(strstr(line,pattern))
						cnt++;
				}			
			}
		}
		if(opt == 'c'){
			printf(MAKE_MAGENTA"%s"MAKE_BLUE":"RESET_COLOR"%d\n",argv[i],cnt);
		}
		i++;
		free(line);
		close(fd);
	}
}


//-w " " Select only those lines containing matches that form whole words
void find_word(int argc,char *argv[],char cflag){
	char *line, *tline, *pattern, ch, *tok;
	int fd, size = 100, j = 0,flag,c=0,f;
	fd = open(argv[3],O_RDONLY);
	if(fd == -1) {
		perror("cp: can't open file");
		exit(errno);
	}
	pattern = (char *)malloc(sizeof(char)*size);
	tok = (char *)malloc(sizeof(char)*size);
	line = (char *)malloc(sizeof(char)*size);
	tline = (char *)malloc(sizeof(char)*size);
	strcpy(pattern,argv[2]);
	while(read(fd,&ch,sizeof(ch))){
		if(ch != '\n'){
			line[j] = ch;
			j++;
			if(j >= size - 1){
				size = size * 2;
				line = (char*)realloc(line,size);
				tline = (char*)realloc(tline,size);
				tok = (char*)realloc(tok,size);
			}
		}
		else{
			line[j] = '\0';
			j = 0;
			strcpy(tline,line);
			tok = strtok(tline," ");
			flag = 0;
			while(tok != NULL){
				if(strcmp(tok,pattern) == 0){
					c=c+1;
					flag = 1;
				}
				tok = strtok(NULL," ");
			}
			if(flag == 1){
				if(cflag=='c')
					f=1;
				else
					color(line,pattern);	
			}
		}
	}
	if(f==1)
		printf("%d\n",c);	
	free(line);
	close(fd);
}

//-m Stop reading a file after NUM matching lines
void find_maxcount(int argc,char *argv[]){
	char *line, ch, *pattern;
	int fd, size = 100;
	int j = 0;
	int num = atoi(argv[2]);
	pattern = (char *)malloc(sizeof(char)*size);
	line = (char *)malloc(sizeof(char)*size);
	strcpy(pattern,argv[3]);
	fd = open(argv[4],O_RDONLY);
	if(fd == -1) {
		perror("cp: can't open file");
		exit(errno);
	}
	while(read(fd,&ch,sizeof(char))){
		if(num > 0){
			if(ch != '\n'){
				line[j] = ch;
				j++;
				if(j >= size - 1){
					size = size * 2;
					line = (char *)realloc(line,size);
				}
			}
			else{
				line[j] = '\0';
				j = 0;
				if(strstr(line,pattern)){
					num--;	
					color(line,pattern);
				}
			}
		}
		else{
			break;
		}	
	}
	free(line);
	free(pattern);
	close(fd);
}
//-c Suppress normal output; instead print a count of matching lines for each input file
void find_count(int argc,char *argv[]){
	char *line, *pattern, ch;
	int fd;
	int c = 0, size = 100, j = 0;
	fd = open(argv[3],O_RDONLY);
	if(fd == -1) {
		perror("cp: can't open file");
		exit(errno);
	}
	pattern = (char *)malloc(sizeof(char)*size);
	strcpy(pattern,argv[2]);
	line = (char *)malloc(sizeof(char)*size);
	while(read(fd,&ch,sizeof(char))){
		if(ch != '\n'){
			line[j] = ch;
			j++;
			if(j >= size - 1){
				size = size * 2;
				line = (char *)realloc(line,size);
			}
		}
		else{
			line[j] = '\0';
			j = 0;
			if(strstr(line,pattern)){
				c++;
			}
		}
	}
	printf("%d\n",c);
	free(line);
	close(fd);
}

//-n Prefix each line of output with the line number within its input file
void find_line_num(int argc,char *argv[]){
	char *line, *pattern, ch;
	int fd;
	int c = 0, size = 100 , j = 0;
	fd = open(argv[3],O_RDONLY);
	if(fd == -1) {
		perror("cp: can't open file");
		exit(errno);
	}
	pattern = (char *)malloc(sizeof(char)*size);
	strcpy(pattern,argv[2]);
	line = (char *)malloc(sizeof(char)*size);
	while(read(fd,&ch,sizeof(char))){
		if(ch != '\n'){
			line[j] = ch;
			j++;
			if(j >= size - 1){
				size = size * 2;
				line = (char *)realloc(line,size);
			}
		}
		else{
			line[j] = '\0';
			c++;
			if(strstr(line,pattern)){
				printf(MAKE_GREEN"%d"MAKE_BLUE":"RESET_COLOR,c);
					color(line,pattern);
			}
			j = 0;
		}
	}
	free(line);
	close(fd);
}


