all:	make
	
make: 	grep_options.h grep_options.o f.o usage.o color.o project.o 
	gcc -Wall grep_options.h f.c usage.c color.c r.c grep_options.c project.c -o project

grep_options.o: grep_options.c
		gcc -c grep_options.c

project.o:	project.c
	gcc -c project.c
	
f.o:	f.c
	gcc -c f.c	

usage.o: usage.c
	 gcc -c usage.c
	
color.o: color.c
	 gcc -c color.c				
clean:
	rm -rf project *o make	
