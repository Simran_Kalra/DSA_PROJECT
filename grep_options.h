#include<sys/types.h>
#include<sys/stat.h>
#include<stdlib.h>
#include<unistd.h>
#include<fcntl.h>
#include<string.h>
#include<dirent.h>
#include<stdio.h>
#include<errno.h>
#define RESET_COLOR "\e[m" 
#define MAKE_GREEN "\e[32m" 
#define MAKE_BLUE "\e[36m" 
#define MAKE_RED "\e[31m"
#define MAKE_MAGENTA "\e[35m"
 
void find_insensitive(int argc,char *argv[],char flag);
void find_match(int argc,char *argv[],int Hflag);
void find_mf(int argc,char *argv[],char opt);
void find_invert(int argc,char *argv[],char flag);
void find_word(int argc,char *argv[],char flag);
void find_maxcount(int argc,char *argv[]);
void find_count(int argc,char *argv[]);
void find_line_num(int argc,char *argv[]);
void usage(int argc,char *argv[]);
void recur(int argc,char *argv[],int hflag,char flag);
void recursive(char *dname,char *search,int hflag,char flag);
void find_mpattern(int argc,char *agv[],int eflag,int f);
void read_mpattern(int argc,char *argv[]);
void find_offset(int argc,char *argv[]);
void color(char *line,char *pattern);
