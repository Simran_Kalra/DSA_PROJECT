#include<stdio.h>
#include<getopt.h>
#include"grep_options.h"
int main(int argc,char *argv[]){
	char opt, flag = '\0';
	int cflag = 0, iflag = 0, hflag = 0, Hflag = 0, rflag = 0, wflag = 0, vflag = 0, nflag = 0, mflag = 0, bflag = 0, fflag = 0, eflag = 0; 	int qflag = 0, f = 0;
	while((opt = getopt(argc,argv,"irecmnwvfhHbq:")) != -1){
		switch(opt){
			case 'i':
				iflag = 1;
				break;
			case 'c':
				cflag = 1;
				break;
			case 'm':
				mflag = 1;
				break;
			case 'v':
				vflag = 1;
				break;
			case 'n':
				nflag = 1;
				break;	
			case 'w':
				wflag = 1;
				break;
			case 'r':
				rflag = 1;
				break;	
			case 'H':
				Hflag = 1;	
				break;
			case 'e':
				eflag = 1;
				break;
			case 'f':
				fflag = 1;
				break;	
			case 'b':
				bflag = 1;
				break;	
			case 'h':
				hflag = 1;
				break;	
			case 'q':
				qflag = 1;
				break;				
			default:
				printf("Invalid Option!");	
		}
	}
	if(qflag == 1){
		return 0;
	}
	else if(argc <= 2){
		usage(argc,argv);
	}
	
	else if(argc == 3){
		find_match(argc,argv,hflag);
	}
	else if(argc == 5 && hflag == 1){
		recur(argc,argv,hflag,flag);
	}
	else if(argc == 6 && eflag == 1){
		f=1;
		find_mpattern(argc,argv,eflag,f);
	}
	else if(argc > 4 && strlen(argv[2]) != 1){
		opt = argv[1][1];
		find_mf(argc,argv,opt);
	}
	else if(iflag == 1){
		if(nflag == 1)
			flag = 'n';
		if(vflag == 1)
			flag = 'v';
		find_insensitive(argc,argv,flag);
	}
	else if(rflag == 1){
		if(nflag == 1)
			flag = 'n';
		else if(cflag == 1)
			flag = 'c';
		else if(vflag == 1)
			flag = 'v';	
		recur(argc,argv,hflag,flag);		
	}
	else if(vflag == 1){
		if(cflag == 1)
			flag = 'c';
		if(nflag == 1)
			flag = 'n';	
		find_invert(argc,argv,flag);
	}
	else if(wflag == 1){
		if(cflag == 1)
			flag = 'c';
		find_word(argc,argv,flag);
	}
	else if(cflag == 1){
		find_count(argc,argv);
	}
	else if(mflag == 1){
		find_maxcount(argc,argv);
	}
	else if(Hflag == 1){
		Hflag = 1;
		find_match(argc,argv,Hflag);	
	}
	else if(nflag == 1){
		find_line_num(argc,argv);
	}
	else if(bflag == 1){
		find_offset(argc,argv);
	}
	else if(eflag == 1){
		find_mpattern(argc,argv,eflag,f);
	}
	else if(fflag == 1){
		read_mpattern(argc,argv);
	}
	else{
		printf("\nInvalid Option");
	}
	return 0;
}
