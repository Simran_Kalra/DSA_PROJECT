# TOPIC - **GREP**<br>
### SIMRAN KALRA - 141708006<br>
#### DESCRIPTION

Grep is a Linux Command. Grep searches the named input FILEs for lines containing a match to the given PATTERN.

#### OPTIONS<br>
-r : Read all files under each directory, recursively.<br>
-i : Ignore case distinctions in both the pattern and the input files.<br>
-v : Invert the sense of matching, to select nonmatching lines.<br>
-f : Obtain the pattern from file.<br>
-w : Select only those lines containing matches that form whole words.<br>
-c : Suppress normal output; instead print a count of matching lines for each input file.<br>
-m : Stop reading a file after NUM matching lines.<br>
-b : Print the byte offset within the input file before each line of output.<br>
-q : Quiet; suppress normal output.<br>
-H : Print the file name for each match.<br>
-h : Suppress the prefixing of filenames on output when multiple files are searched.<br>
-e : Use pattern as the pattern; useful to protect patterns beginning with -.<br>

```
SYNOPSIS
        grep [OPTIONS] PATTERN [FILE...]
        grep [OPTIONS] [-e PATTERN]...  [-f FILE]...  [FILE...]
```

### **EXAMPLE**

#### **test.txt**
```
This is the first line of the file.
This Is The Second Line Of The File.
This is the last line.
```

#### **Syntax**
```
grep <string> <filename>
```

#### **Command**
```
grep File test.txt
```

#### **Output**
```
This Is The Second Line Of The File.
```