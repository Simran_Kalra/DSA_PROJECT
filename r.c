#include "grep_options.h"
//-r & -r -h & -rn Read all files under each directory, recursively
void recursive(char *dname,char *search,int hflag,char nflag){
	int f[100], size[100],i = 0,ln=0,c=0,cflag=0;
	char s[2] = "\n", *token, *a;
	struct dirent *d;
	DIR *dir;
	dir = opendir(dname);
	if(dir != NULL){
		while((d = readdir(dir))){

			if(d->d_type == DT_DIR){
				if(strcmp(d->d_name,".") == 0 || strcmp(d->d_name,"..") == 0);
				else{
					strcat(dname,"/");
					strcat(dname,d->d_name);
					recursive(dname,search,hflag,nflag);
				}
			}
			else if((f[i]=open(d->d_name,O_RDONLY))){
				ln=0;
				c=0;
				size[i]=lseek(f[i],0,SEEK_END);
				a=(char *)malloc(sizeof(char)*size[i]);
				token=(char *)malloc(sizeof(char)*size[i]);
				lseek(f[i],0,0);
				read(f[i],a,sizeof(char)*size[i]);
				token=strtok(a,s);
				while(token!=NULL){
					ln++;
					if(strstr(token,search) != NULL && hflag != 1){
						c++;
						if(nflag=='n'){
							printf(MAKE_MAGENTA"%s"MAKE_BLUE":"MAKE_GREEN"%d"MAKE_BLUE":",d->d_name,ln);
							color(token,search);
						}
						else if(nflag=='c'){
							cflag=1;
						}
						else if(nflag=='\0'){
							printf(MAKE_MAGENTA"%s"MAKE_BLUE":",d->d_name);
							color(token,search);
						}
					}
					else if(nflag=='v'){
						if(!strstr(token,search)){
							printf(MAKE_MAGENTA"%s"MAKE_BLUE":"RESET_COLOR"%s\n",d->d_name,token);
						}
					}
					else{
						if(strstr(token,search) != NULL && hflag == 1){
							color(token,search);
						}	
					}
					token=strtok(NULL,s);
				}
				if(cflag==1){
					printf(MAKE_MAGENTA"%s"MAKE_BLUE":"RESET_COLOR"%d\n",d->d_name,c);
				}
				close(f[i]);
				free(a);
			}
			i++;
		}
	}
	closedir(dir);
}

void recur(int argc,char *argv[],int hflag,char nflag){
	char *dname, *key;
	int size = 100;
	dname = (char *)malloc(sizeof(char)*size);
	key = (char *)malloc(sizeof(char)*size);
	if(hflag == 1){
		strcpy(dname,argv[4]);
		strcpy(key,argv[3]);
	}
	else{
		strcpy(dname,argv[3]);
		strcpy(key,argv[2]);
	}
	//printf("%s",dname);
	// 	printf("%s",key);
	recursive(dname,key,hflag,nflag);
}

